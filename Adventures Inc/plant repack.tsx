<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="plant repack" tilewidth="32" tileheight="32" tilecount="169" columns="13">
 <image source="plant repack.png" width="416" height="416"/>
 <tile id="29">
  <objectgroup draworder="index" id="2">
   <object id="1" x="27.0909" y="31.4545">
    <polygon points="0,0 2.90909,-7.63636 3.45455,-12.7273 -2.36364,-21.6364 -18.3636,-25.2727 -4,-26.1818 4.72727,-18.9091 5.27273,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="30">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.81818" y="32.7273">
    <polygon points="0,0 5.45455,-10.3636 3.81818,-15.4545 10,-17.6364 -2.54545,-16.1818 -2.54545,-0.909091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="33">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.45455" y="32.5455">
    <polygon points="0,0 11.6364,0 9.63636,-19.8182 18.5455,-33.0909 -10,-32 0.545455,-16.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="37">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.6364" y="11.6364">
    <polygon points="0,0 -8,1.63636 -10.5455,15.0909 -17.0909,16.9091 -13.2727,20 0.363636,20.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="38">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="10.3636">
    <polygon points="0,0 9.27273,4.18182 17.0909,21.0909 -1.27273,21.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="42">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.9091" y="13.8182">
    <polygon points="0,0 -6.72727,0.181818 -0.363636,-6.54545 -6.54545,-14 -0.727273,-15.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="43">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="17.4545">
    <polygon points="0,0 24.5455,-3.27273 1.81818,-19.2727 -1.45455,-17.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="46">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="14.9091">
    <polygon points="0,0 23.6364,1.63636 17.2727,-16 8.54545,-14.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="50">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.18182" y="5.81818">
    <polygon points="0,0 20.9091,6.72727 24.1818,14.5455 26.5455,13.4545 26,-6.90909 10.5455,-6 8.18182,-3.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="51">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="19.0909">
    <polygon points="0,0 5.81818,-8.18182 21.4545,-4.90909 15.6364,-13.2727 25.8182,-14.3636 20.1818,-19.8182 0.363636,-19.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="82">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="31.8182">
    <polygon points="0,0 -0.363636,-33.0909 18.9091,-31.8182 18.9091,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="92">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4.90909" y="32.3636">
    <polygon points="0,0 16.5455,0.181818 17.0909,-23.2727 26.5455,-29.0909 26.1818,-32.1818 -5.81818,-33.6364 -6.18182,-30.9091 0.909091,-27.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="95">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.63636" y="32.1818">
    <polygon points="0,0 16.9091,0 16.3636,-32.7273 0.363636,-32.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="98">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.18182" y="32.3636">
    <polygon points="0,0 16.7273,0 15.8182,-32.5455 -1.45455,-33.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="104">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.8182" y="8.54545">
    <polygon points="0,0 -3.45455,2.90909 -2.54545,6.18182 1.45455,6.54545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="105">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.09091" y="18.9091">
    <polygon points="0,0 5.45455,-2.36364 7.81818,2.18182 12.3636,-1.09091 17.0909,0.363636 18.1818,-2.90909 21.0909,-3.27273 22,-5.63636 24.5455,-5.45455 25.6364,-6.72727 24.1818,-12.7273 20.3636,-14.5455 19.2727,-18.9091 1.81818,-20.1818 2.90909,-14.7273 -3.27273,-9.27273 -2.18182,-4.90909 3.27273,-5.81818 -1.63636,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="108">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.27273" y="0.909091">
    <polygon points="0,0 -3.63636,5.63636 -4.54545,11.2727 2.36364,11.4545 2.72727,15.2727 9.09091,11.2727 14.7273,14.3636 14.3636,9.45455 18.7273,9.27273 15.0909,2 13.8182,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="111">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4.36364" y="12.7273">
    <polygon points="0,0 6.72727,-1.27273 9.09091,2.36364 12.7273,0.363636 19.0909,2.72727 19.0909,-1.81818 24.1818,-2.90909 18.5455,-13.2727 2.72727,-12.9091 3.63636,-6 0.727273,-6.18182 2.36364,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="144">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18.9091" y="28.5455">
    <polygon points="0,0 5.63636,4.18182 12.9091,3.27273 14.7273,-9.45455 9.45455,-19.0909 1.27273,-7.09091 1.27273,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="145">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="32.3636">
    <polygon points="0,0 8.90909,-17.2727 6.90909,-21.0909 -1.63636,-20.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="157">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18.3636" y="27.0909">
    <polygon points="0,0 14.3636,-4.18182 13.6364,-27.8182 3.63636,-28.3636 6.36364,-19.8182 -4.18182,-13.0909 2.54545,-7.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="158">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="24.3636">
    <polygon points="0,0 6.72727,-0.909091 10.7273,3.27273 9.63636,-7.63636 16.5455,-7.09091 3.27273,-24.9091 1.63636,-25.0909"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
