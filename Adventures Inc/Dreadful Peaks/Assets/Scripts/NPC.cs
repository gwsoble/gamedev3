﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
	public GameObject dialogBox;
	public Text dialogText;
	public string dialog;
	public bool playerInRange;

    // Start is called before the first frame update
    void Start()
    {
		dialog = "Hello!  I'm a prefab!  I can show you how easy it is to implement NPC behavior!";
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && playerInRange)
		{
			if (dialogBox.activeInHierarchy)
			{
				dialogBox.SetActive(false);
			}
			else 
			{
				dialogBox.SetActive(true);
				dialogText.text = dialog;
			}
		}
    }

	private void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.CompareTag("Player"))
		{
			playerInRange = true;
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.CompareTag("Player")) {
			playerInRange = false;
			dialogBox.SetActive(false);
		}
	}
}
