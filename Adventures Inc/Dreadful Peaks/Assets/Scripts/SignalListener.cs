﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SignalListener : MonoBehaviour
{
	public Signal signal;
   public void OnSignalRaised()
	{

	}

	private void OnEnable() {
		signal.RegisterListener(this);
	}

	private void OnDisable() {
		signal.DeregisterListener(this);
	}
}
