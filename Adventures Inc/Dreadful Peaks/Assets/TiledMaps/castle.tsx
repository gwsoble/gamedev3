<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="castle" tilewidth="32" tileheight="32" tilecount="152" columns="8">
 <image source="castle.png" width="256" height="608"/>
 <tile id="19">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="31.8182">
    <polygon points="0,0 -0.181818,-31.8182 30.7273,-31.6364 31.0909,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="0.363636">
    <polygon points="0,0 31.6364,0.181818 31.0909,31.6364 -0.909091,32"/>
   </object>
   <object id="2" x="31.6364" y="32">
    <polygon points="0,0 -31.8182,0.545455 -31.2727,-31.8182 0.545455,-31.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="0.363636">
    <polygon points="0,0 31.4545,0 31.6364,32 -0.909091,31.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="27">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="32.5455">
    <polygon points="0,0 30.9091,-0.363636 31.2727,-33.2727 -0.363636,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="28">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.09091" y="-0.363636">
    <polygon points="0,0 34,-0.181818 33.8182,32.3636 -0.181818,32.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="29">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.45455" y="0.363636">
    <polygon points="0,0 30.7273,0 30.5455,33.0909 -2.36364,31.8182 -1.63636,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="136">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="19.0909">
    <polygon points="0,0 6.90909,-3.27273 24.1818,-3.09091 32.1818,1.09091 26.9091,8.54545 23.2727,8.72727 23.2727,12.9091 7.81818,12.9091 7.09091,7.09091 2.72727,7.09091 -0.181818,1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="144">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.09091" y="9.27273">
    <polygon points="0,0 17.4545,-0.181818 16.7273,-10 1.63636,-9.81818"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
