<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="lpc_castle" tilewidth="32" tileheight="32" tilecount="150" columns="15">
 <image source="lpc_castle.png" width="480" height="320"/>
 <tile id="148">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="31.6364">
    <polyline points="0,0 32.3636,0.181818"/>
   </object>
   <object id="2" x="0.545455" y="-0.363636">
    <polyline points="0,0 31.0909,0.727273"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
