<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="Orc_Build" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="adobe-2.png" width="512" height="512"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.8182" y="-0.181818">
    <polygon points="0,0 -27.6364,0.545455 -30.9091,4.36364 -31.2727,31.4545 0.181818,32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="1.09091">
    <polygon points="0,0 -32.9091,-0.363636 -32.9091,30.1818 -1.27273,30"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="32.5455">
    <polygon points="0,0 32.7273,0.181818 32,-32.7273 0,-33.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.2727" y="0.727273">
    <polygon points="0,0 -30.7273,-0.363636 -30.3636,31.2727 0.909091,31.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="32.5455">
    <polygon points="0,0 31.4545,-0.181818 32.3636,-33.0909 -0.363636,-33.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="32">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="32">
    <polyline points="0,0 30.1818,-0.181818"/>
   </object>
   <object id="2" x="31.6364" y="0.363636">
    <polygon points="0,0 -31.8182,-0.363636 -31.4545,32.3636 0.363636,31.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="34">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="32.1818">
    <polygon points="0,0 30.9091,-0.545455 31.2727,-32.3636 -0.545455,-32.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="48">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="31.6364">
    <polygon points="0,0 30.7273,0.545455 30.3636,-32.1818 -1.27273,-32.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="49">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="0.545455">
    <polygon points="0,0 31.0909,-0.363636 31.2727,30.1818 1.45455,31.6364 -1.27273,31.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="50">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="0">
    <polygon points="0,0 30.9091,-0.181818 31.8182,31.6364 -0.545455,32.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="64">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="28.7273">
    <polygon points="0,0 1.81818,2.54545 31.6364,3.27273 30.5455,-28.3636 -1.27273,-28.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="65">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="31.6364">
    <polygon points="0,0 32.1818,0.727273 31.4545,-32.5455 -0.909091,-32.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="66">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.09091" y="31.0909">
    <polygon points="0,0 34.1818,1.27273 33.0909,-31.2727 0.909091,-31.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="89">
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="0.909091">
    <polygon points="0,0 27.8182,-0.545455 27.8182,31.6364 0.181818,31.8182 -0.181818,15.2727 -3.81818,15.6364 -3.81818,6.90909 -0.363636,7.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="90">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="0.363636">
    <polygon points="0,0 31.6364,-0.545455 32,32.3636 -0.727273,31.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="91">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.545455" y="32.3636">
    <polygon points="0,0 27.4545,0 28.9091,-16.7273 33.6364,-15.2727 32.9091,-24.1818 29.2727,-24.1818 28.9091,-33.0909 0.545455,-32.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="96">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="4.72727">
    <polygon points="0,0 3.81818,-4.54545 31.0909,-4.18182 31.0909,26.5455 -1.09091,27.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="97">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="0.727273">
    <polygon points="0,0 -0.181818,30.9091 31.4545,31.4545 31.4545,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="98">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="32.1818">
    <polygon points="0,0 32.1818,-0.181818 31.6364,-32.1818 -1.27273,-32.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="99">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="32">
    <polygon points="0,0 32.7273,0 31.8182,-32.3636 0.727273,-32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="105">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.27273" y="-0.181818">
    <polygon points="0,0 0,8.54545 -3.45455,8.36364 -3.63636,18 -0.181818,17.6364 0,32.3636 28.3636,32.9091 28.7273,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="107">
  <objectgroup draworder="index" id="2">
   <object id="1" x="26.7273" y="32.3636">
    <polygon points="0,0 1.09091,-16.7273 5.27273,-15.8182 5.45455,-24 0.363636,-24.5455 -0.363636,-33.2727 -26.5455,-32.5455 -27.2727,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="112">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="32.3636">
    <polygon points="0,0 33.2727,0.181818 31.6364,-32.7273 -0.545455,-33.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="32.1818">
    <polygon points="0,0 33.8182,-0.181818 32.7273,-32.7273 -1.09091,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="114">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="32.7273">
    <polygon points="0,0 32.1818,-0.181818 32,-33.2727 -0.727273,-33.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="115">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.545455" y="32.5455">
    <polygon points="0,0 32.7273,-0.363636 32.7273,-33.2727 0,-32.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="121">
  <objectgroup draworder="index" id="2">
   <object id="1" x="13.0909" y="32.1818">
    <polygon points="0,0 4.90909,0 7.81818,-13.0909 11.8182,-13.4545 14,-9.45455 15.6364,-9.27273 16.7273,-14.1818 18.9091,-14 18.3636,-32.1818 -8.90909,-32.3636 -10.9091,-26.5455 -9.27273,-21.2727 -6.54545,-14 0.181818,-12.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="122">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="18">
    <polygon points="0,0 2.72727,4.54545 31.6364,4.72727 30.7273,-18.9091 0.363636,-18.9091 -0.727273,-2.36364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="123">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="19.2727">
    <polygon points="0,0 13.0909,1.45455 14,14 18.7273,13.0909 19.8182,3.09091 24.5455,-2.54545 32.5455,-1.45455 32.9091,-11.8182 27.0909,-10.9091 27.6364,-19.4545 -0.727273,-19.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="128">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="0.909091">
    <polygon points="0,0 0.545455,30.9091 31.8182,31.4545 31.6364,-1.81818 -0.545455,-1.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="129">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="-0.545455">
    <polygon points="0,0 0.727273,31.6364 32.3636,32.5455 32.1818,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="131">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="0.545455">
    <polygon points="0,0 0.181818,31.0909 32.5455,32.1818 32.7273,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
