<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="obj_misk_atlas" tilewidth="32" tileheight="32" tilecount="1024" columns="32">
 <image source="obj_misk_atlas.png" width="1024" height="1024"/>
 <tile id="200">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.8182" y="32.3636">
    <polygon points="0,0 -5.63636,-0.181818 -6.18182,-3.09091 -1.27273,-6.18182 -0.181818,-14 -8.18182,-23.8182 -18.3636,-26.1818 -9.63636,-27.8182 -12.7273,-31.2727 -8.18182,-32.1818 -4.36364,-22.7273 1.81818,-18.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="201">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.54545" y="32">
    <polygon points="0,0 5.09091,-9.45455 1.45455,-15.4545 -2.90909,-14.9091 -3.27273,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="204">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.81818" y="32.7273">
    <polygon points="0,0 10.7273,-0.363636 9.27273,-18.7273 13.4545,-19.4545 5.81818,-28 0.363636,-26.7273 0.727273,-20.5455 -4.54545,-27.0909 -7.63636,-25.2727 -1.27273,-20.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="296">
  <objectgroup draworder="index" id="2">
   <object id="1" x="26.9091" y="31.6364">
    <polygon points="0,0 4.54545,-4.90909 4.18182,-11.8182 -6.90909,-22.9091 5.45455,-24 5.81818,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="297">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.72727" y="31.0909">
    <polygon points="0,0 4.72727,-10.3636 3.45455,-14.3636 16.3636,-18.7273 12.1818,-32 -3.45455,-31.8182 -2.90909,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="300">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.90909" y="32">
    <polygon points="0,0 11.6364,0.545455 9.45455,-29.0909 -6,-28.1818 -7.45455,-23.2727 0.727273,-16.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="328">
  <objectgroup draworder="index" id="2">
   <object id="1" x="26.5455" y="14.5455">
    <polygon points="0,0 7.45455,-1.45455 5.81818,-15.4545 1.45455,-15.0909 5.45455,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="329">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.45455" y="17.2727">
    <polygon points="0,0 26,-2.90909 6.72727,-18.3636 1.81818,-17.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="332">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="16.3636">
    <polygon points="0,0 24.3636,1.27273 17.2727,-17.4545 7.45455,-17.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="352">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.09091" y="32.5455">
    <polygon points="0,0 -0.727273,-8.54545 12.3636,-16.9091 19.6364,-15.0909 25.2727,-9.63636 28.3636,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="353">
  <objectgroup draworder="index" id="2">
   <object id="1" x="30.9091" y="-0.181818">
    <polygon points="0,0 -13.6364,8.54545 -13.4545,28.3636 0.545455,32.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="354">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10.5455" y="31.4545">
    <polygon points="0,0 3.81818,-0.909091 3.81818,-21.4545 0.181818,-27.4545 -3.63636,-28.9091 -11.8182,-33.0909 -11.4545,1.63636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="355">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.81818" y="32.1818">
    <polygon points="0,0 1.45455,-4.90909 10.1818,-11.2727 15.8182,-10.9091 15.0909,-17.4545 22.7273,-24.1818 22.5455,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="356">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="7.27273">
    <polygon points="0,0 7.09091,-3.09091 11.2727,-2 19.4545,4.72727 22.5455,14.1818 22.3636,24.9091 0.363636,24.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="357">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.1818" y="5.27273">
    <polygon points="0,0 2.90909,-2.54545 8.54545,1.81818 7.45455,20.5455 0,21.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="361">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="30.7273">
    <polyline points="0,0 -0.363636,-30.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="363">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.6364" y="-0.545455">
    <polyline points="0,0 -0.727273,31.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="364">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.27273" y="11.8182">
    <polyline points="0,0 0.545455,21.0909"/>
   </object>
   <object id="2" x="31.6364" y="12.7273">
    <polyline points="0,0 -0.909091,18.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="384">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.45455" y="0.909091">
    <polygon points="0,0 -0.181818,10.1818 11.4545,15.4545 25.8182,13.6364 27.8182,-0.909091 0.181818,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="385">
  <objectgroup draworder="index" id="2">
   <object id="1" x="15.6364" y="0.909091">
    <polygon points="0,0 -13.2727,8.54545 -14.1818,27.4545 0.909091,30.9091 13.8182,28.7273 13.6364,6"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="387">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.09091" y="0.181818">
    <polygon points="0,0 -0.909091,20 17.6364,20.5455 23.8182,27.2727 22.5455,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="388">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.909091" y="-0.181818">
    <polygon points="0,0 22,0.181818 27.2727,12 25.8182,26.3636 0.727273,26.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="389">
  <objectgroup draworder="index" id="2">
   <object id="1" x="6.18182" y="5.09091">
    <polygon points="0,0 8.72727,-6.54545 16.3636,0.909091 17.6364,22.5455 12.7273,24.3636 0,22.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="393">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.45455" y="31.0909">
    <polyline points="0,0 -0.545455,-32"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="395">
  <objectgroup draworder="index" id="2">
   <object id="1" x="29.4545" y="1.27273">
    <polyline points="0,0 0.181818,29.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="396">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="0.545455">
    <polyline points="0,0 0.363636,31.4545"/>
   </object>
   <object id="2" x="31.4545" y="0.727273">
    <polyline points="0,0 0.727273,30.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="416">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.45455" y="32.5455">
    <polygon points="0,0 0.545455,-13.8182 30.7273,-13.6364 30.9091,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="417">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="1.27273">
    <polygon points="0,0 -11.2727,0.363636 -15.4545,2.36364 -15.8182,28.9091 -0.181818,27.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="418">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.45455" y="28">
    <polygon points="0,0 16.5455,4.18182 17.0909,-24.9091 0.727273,-27.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="419">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="1.27273">
    <polygon points="0,0 -15.2727,2.54545 -15.2727,17.2727 -31.8182,20.3636 -31.6364,31.6364 0.181818,30.7273 -1.45455,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="420">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="1.45455">
    <polygon points="0,0 14.9091,2 15.2727,11.4545 31.8182,14.5455 32,31.6364 0.909091,30.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="425">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="31.2727">
    <polyline points="0,0 0.909091,-31.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="427">
  <objectgroup draworder="index" id="2">
   <object id="1" x="28" y="0">
    <polyline points="0,0 1.27273,31.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="428">
  <objectgroup draworder="index" id="3">
   <object id="2" x="32" y="0.727273">
    <polyline points="0,0 -0.363636,30.1818"/>
   </object>
   <object id="3" x="0.909091" y="1.63636">
    <polyline points="0,0 -0.545455,27.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="448">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.81818" y="14.5455">
    <polygon points="0,0 29.8182,1.09091 30.1818,-13.8182 -0.545455,-14.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="449">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.45455" y="3.63636">
    <polygon points="0,0 30.5455,0 29.6364,28 -1.09091,26.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="451">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="0.363636">
    <polygon points="0,0 0.545455,16.9091 26.5455,15.6364 27.2727,29.6364 32.3636,29.8182 31.6364,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="452">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="28.7273">
    <polygon points="0,0 24.3636,2.90909 27.0909,-19.6364 29.2727,-18.1818 28.1818,-30 -0.909091,-28.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="460">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.3636" y="12">
    <polyline points="0,0 0.909091,20"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="462">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16.9091" y="32.1818">
    <polyline points="0,0 0.363636,-21.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="480">
  <objectgroup draworder="index" id="2">
   <object id="1" x="30.1818" y="32.5455">
    <polygon points="0,0 0.363636,-6.54545 -5.27273,-9.63636 -21.4545,-9.45455 -29.8182,-6.36364 -26.3636,0 -1.45455,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="481">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.3636" y="8">
    <polygon points="0,0 -7.09091,-0.727273 -15.8182,3.45455 -12.7273,6.90909 -14.3636,12.1818 -13.4545,15.8182 -17.2727,17.2727 -9.27273,24.1818 -0.545455,23.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="482">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="6">
    <polygon points="0,0 11.6364,1.27273 13.8182,4.18182 15.8182,20 10.3636,24.1818 0.363636,25.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="483">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.1818" y="7.09091">
    <polygon points="0,0 -14.7273,3.27273 -14,14.3636 -31.4545,20 -27.8182,24.7273 0.363636,25.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="484">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="6.72727">
    <polygon points="0,0 11.2727,2 11.8182,5.27273 14.5455,19.4545 -1.09091,25.8182 -2,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="492">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16.7273" y="0">
    <polyline points="0,0 -3.09091,31.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="494">
  <objectgroup draworder="index" id="2">
   <object id="1" x="17.6364" y="31.8182">
    <polyline points="0,0 -0.545455,-32.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="512">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.54545" y="-0.363636">
    <polygon points="0,0 28.5455,0 28.7273,10.5455 17.6364,15.8182 9.63636,16.3636 -1.27273,11.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="513">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="12">
    <polygon points="0,0 10.3636,-6 27.8182,-4.18182 29.4545,-2 30.7273,13.4545 23.0909,20.3636 8,20 -0.181818,13.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="515">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2.36364" y="0.363636">
    <polygon points="0,0 -1.63636,10 5.45455,15.2727 23.8182,14.9091 26.5455,11.4545 28,16 24.9091,18.7273 30,22.1818 30,2.90909 26.5455,4.72727 27.2727,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="516">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.45455" y="23.2727">
    <polygon points="0,0 9.27273,3.45455 20.3636,2.90909 28.3636,-3.09091 27.2727,-18.9091 21.4545,-22.5455 4.72727,-21.8182 1.09091,-18.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="524">
  <objectgroup draworder="index" id="2">
   <object id="1" x="14.1818" y="0">
    <polyline points="0,0 -0.727273,24.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="526">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18.3636" y="25.0909">
    <polyline points="0,0 -0.727273,-26.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="545">
  <objectgroup draworder="index" id="2">
   <object id="1" x="33.0909" y="7.63636">
    <polygon points="0,0 -8.36364,-0.545455 -16,4 -12.7273,8.90909 -14.7273,16 -17.6364,17.6364 -8.90909,24.1818 -0.909091,24.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="546">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="6.72727">
    <polygon points="0,0 6.36364,0.545455 14.1818,3.81818 15.0909,19.6364 0.909091,25.4545 -1.09091,25.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="548">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="7.63636">
    <polygon points="0,0 13.4545,1.81818 14.3636,17.4545 16.3636,17.4545 16,19.4545 0,24.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="576">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="9.63636">
    <polygon points="0,0 6.18182,7.09091 25.4545,6 31.4545,-0.181818 28.5455,-1.63636 29.2727,-9.81818 2.90909,-10.1818 1.81818,-7.27273 2.72727,-2.18182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="577">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.727273" y="26.1818">
    <polygon points="0,0 7.45455,4.54545 24,6.36364 32.3636,0.181818 30,-16.7273 25.6364,-20.3636 11.2727,-19.2727 2,-15.0909 5.63636,-11.8182 3.09091,-6.72727 4.18182,-3.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="579">
  <objectgroup draworder="index" id="2">
   <object id="1" x="3.81818" y="-0.181818">
    <polygon points="0,0 -0.727273,14.9091 21.0909,14 24.9091,20.7273 28,22.3636 27.8182,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="580">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="4.90909">
    <polygon points="0,0 4.36364,-4.36364 23.4545,-2.72727 27.8182,17.2727 12.9091,21.6364 -1.45455,18.3636"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
