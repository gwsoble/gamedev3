<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="interior" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="LPC_house_interior/LPC_house_interior/interior.png" width="512" height="512"/>
 <terraintypes>
  <terrain name="Inside_Walls" tile="0"/>
 </terraintypes>
 <tile id="2" terrain="0,0,0,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131579" y="1.18421" width="32.1053" height="30.6579"/>
  </objectgroup>
 </tile>
 <tile id="3" terrain="0,0,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.394737" y="0.526316" width="31.4474" height="31.3158"/>
  </objectgroup>
 </tile>
 <tile id="4" terrain="0,0,,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.263158" y="1.05263" width="30.7895" height="30.5263"/>
  </objectgroup>
 </tile>
 <tile id="16" terrain="0,,,"/>
 <tile id="17" terrain=",0,,"/>
 <tile id="18">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.131579" y="0.657895" width="31.5789" height="31.3158"/>
  </objectgroup>
 </tile>
 <tile id="19" terrain="0,0,0,0"/>
 <tile id="20">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="32.5455">
    <polygon points="0,0 32,-0.363636 30.7273,-32.3636 -0.909091,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="32" terrain=",,0,"/>
 <tile id="33" terrain=",,,0"/>
 <tile id="34" terrain="0,,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.921053" y="19.8684" width="30.9211" height="11.3158"/>
   <object id="2" x="-1.57895" y="0" width="8.68421" height="18.5526"/>
  </objectgroup>
 </tile>
 <tile id="35" terrain=",,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.657895" y="19.7368" width="31.3158" height="10.9211"/>
  </objectgroup>
 </tile>
 <tile id="36" terrain=",0,0,0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.657895" y="19.8684" width="30.5263" height="11.8421"/>
   <object id="2" x="25.5263" y="0.263158" width="5.52632" height="18.8158"/>
  </objectgroup>
 </tile>
 <tile id="51">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.727273" y="31.4545">
    <polygon points="0,0 0.727273,-20.1818 32.3636,-19.2727 32.5455,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="67">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="19.4545">
    <polygon points="0,0 31.8182,0.181818 32,-19.4545 -0.363636,-19.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="82">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="32.9091">
    <polygon points="0,0 -1.45455,-21.6364 30.7273,-20.5455 31.8182,-1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="96">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="0.545455">
    <polygon points="0,0 0.545455,32 33.2727,31.6364 32.3636,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="97">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.545455" y="0.363636">
    <polygon points="0,0 1.45455,31.8182 32.5455,31.8182 32.3636,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="98">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="-0.545455">
    <polygon points="0,0 32.7273,0 32.7273,32.9091 0.909091,32.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="99">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="0.181818">
    <polygon points="0,0 0.363636,32.5455 32,32.3636 31.2727,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="100">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="32.3636">
    <polygon points="0,0 30.7273,0 31.6364,-16.9091 -1.63636,-16.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="101">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="31.6364">
    <polygon points="0,0 0.363636,-16 32,-15.2727 31.6364,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="102">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="32">
    <polygon points="0,0 0,-16.7273 32,-15.4545 31.8182,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="103">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="32">
    <polygon points="0,0 -0.909091,-16.1818 31.0909,-15.6364 31.8182,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="112">
  <objectgroup draworder="index" id="3">
   <object id="2" x="0.526316" y="0.394737" width="30.7895" height="31.1842"/>
   <object id="3" x="0" y="31.6364">
    <polygon points="0,0 32.1818,0.363636 31.2727,-31.8182 0,-31.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.545455" y="32.1818">
    <polygon points="0,0 32.5455,0.363636 32.3636,-33.2727 0.909091,-32.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="114">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="31.8182">
    <polygon points="0,0 32.7273,0 32.1818,-32.1818 -0.727273,-32.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="115">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="31.6364">
    <polygon points="0,0 31.0909,0.727273 30.5455,-31.8182 -1.09091,-32.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="116">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="25.0909">
    <polygon points="0,0 32.3636,0.909091 33.0909,-26 0.363636,-25.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="117">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="25.8182">
    <polygon points="0,0 31.8182,0.727273 30,-26.9091 0.909091,-26.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="118">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="25.8182">
    <polygon points="0,0 31.8182,0 31.8182,-26 -0.545455,-26.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="128">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="0.363636">
    <polygon points="0,0 -0.545455,32.5455 32,31.6364 32.1818,-0.909091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="129">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.6364" y="11.8182">
    <polygon points="0,0 -11.8182,3.81818 -12.3636,12 -25.2727,13.4545 -28.7273,18 -27.0909,20.7273 0.545455,21.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="130">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32" y="32.5455">
    <polygon points="0,0 -6.72727,-8.90909 -19.0909,-10.1818 -19.2727,-17.2727 -30.9091,-21.8182 -32.3636,-20.3636 -32.1818,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="131">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="31.6364">
    <polygon points="0,0 0.545455,-28 32.5455,-27.6364 32.5455,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="134">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="32">
    <polygon points="0,0 32.7273,0.181818 32.3636,-32.9091 -0.363636,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="144">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="32.3636">
    <polygon points="0,0 31.6364,-0.545455 31.2727,-32.5455 -0.909091,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="145">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.9091" y="10.9091">
    <polygon points="0,0 -4,0.181818 -6.36364,8.54545 -11.6364,11.4545 -20.7273,11.4545 -26,9.45455 -28.9091,6 -28,-11.4545 -1.81818,-11.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="146">
  <objectgroup draworder="index" id="2">
   <object id="1" x="29.8182" y="1.09091">
    <polygon points="0,0 -3.81818,20.3636 -18.7273,22 -24.5455,18.3636 -27.2727,8.90909 -29.8182,8.90909 -30.1818,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="147">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="29.0909">
    <polygon points="0,0 31.4545,0 31.2727,-30 -1.09091,-29.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="150">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="32.5455">
    <polygon points="0,0 32,-0.727273 32,-32.5455 0.181818,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
