<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="mountains-v6" tilewidth="32" tileheight="32" tilecount="5120" columns="64">
 <image source="mountains-v6.png" width="2048" height="2560"/>
 <terraintypes>
  <terrain name="Cliff_Sand" tile="69"/>
  <terrain name="Cliff_Dirt_Tan" tile="81"/>
  <terrain name="Cliff_Dirt_Brown" tile="93"/>
  <terrain name="Cliff_Dirt_Dark" tile="105"/>
  <terrain name="Cliff_Snow" tile="117"/>
  <terrain name="Cliff_White" tile="837"/>
  <terrain name="Cliff_Grey" tile="849"/>
  <terrain name="Cliff_Black" tile="861"/>
  <terrain name="Cliff_Basalt" tile="873"/>
  <terrain name="Cliff_Stone_Tan" tile="885"/>
  <terrain name="Cliff_Earth_Cracked" tile="1605"/>
  <terrain name="Cliff_Grass" tile="1617"/>
  <terrain name="Cliff_Grass_Dark" tile="1629"/>
  <terrain name="Cliff_Mud_Dark" tile="1641"/>
  <terrain name="Cliff_Mudstone_Grey" tile="1653"/>
  <terrain name="Cliff_Sharp_Tan" tile="2498"/>
  <terrain name="Cliff_Sharp_White" tile="2946"/>
  <terrain name="Cliff_Round_Grey" tile="3586"/>
  <terrain name="Cliff_Round_Tan" tile="4034"/>
  <terrain name="Cliff_Oblong_White" tile="2784"/>
  <terrain name="Cliff_Oblong_Grey" tile="3552"/>
  <terrain name="Cliff_Crumble_Sand" tile="2738"/>
  <terrain name="Cliff_Crumble_Dirt_Tan" tile="2741"/>
  <terrain name="Cliff_Crumble_Rock_White" tile="2744"/>
  <terrain name="Cliff_Crumble_Rock_Grey" tile="2747"/>
  <terrain name="Cliff_Crumble_Rock_Black" tile="2750"/>
  <terrain name="Cliff_Scramble_Dirt_Brown" tile="3186"/>
  <terrain name="Cliff_Scramble_Dirt_Dark" tile="3189"/>
  <terrain name="Cliff_Scramble_Rock_White" tile="3192"/>
  <terrain name="Cliff_Scramble_Rock_Grey" tile="3195"/>
  <terrain name="Cliff_Scramble_Rock_Black" tile="3198"/>
  <terrain name="Cliff_Worn_Dirt_Brown_Grass" tile="3622"/>
  <terrain name="Cliff_Worn_Sand" tile="3692"/>
  <terrain name="Cliff_Worn_Dirt_Brown" tile="3698"/>
  <terrain name="Cliff_Tube_Rock_Grey" tile="3704"/>
  <terrain name="Cliff_Tube_Dirt_Brown" tile="3710"/>
  <terrain name="Mound_Rock_White" tile="189"/>
  <terrain name="Mound_Dirt_Tan" tile="573"/>
  <terrain name="Mound_Rock_Grey" tile="957"/>
  <terrain name="Mound_Crumble_Tan" tile="1341"/>
 </terraintypes>
 <tile id="4" terrain=",,,0"/>
 <tile id="5" terrain=",,0,0"/>
 <tile id="6" terrain=",,0,"/>
 <tile id="12">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="30.9091">
    <polygon points="0,0 31.4545,-31.0909 32.1818,-10.5455 21.0909,-1.81818 23.8182,1.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-2" y="9.81818">
    <polygon points="0,0 36.1818,0.545455 33.8182,-10.5455 1.63636,-10.3636"/>
   </object>
   <object id="2" x="22.5455" y="32.7273">
    <polyline points="0,0 1.27273,-8.54545 9.81818,-12.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="1" x="21.8182" y="33.0909">
    <polygon points="0,0 2,-8.72727 11.2727,-13.8182 10.1818,-33.4545 -22.1818,-33.4545 -22.3636,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.1818" y="21.6364">
    <polygon points="0,0 0.181818,-13.0909 -8.72727,-14.9091 -15.6364,-16 -19.4545,-12.3636 -31.6364,-12.5455 -32.3636,10.9091 -8.72727,11.2727 -7.63636,3.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16" terrain=",,,1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.0909" y="32.5455">
    <polyline points="0,0 0.727273,-7.81818 10,-13.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17" terrain=",,1,1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="18.7273">
    <polyline points="0,0 17.8182,0.545455 19.4545,-3.45455 29.6364,0.181818 32.5455,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18" terrain=",,1,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="19.0909">
    <polyline points="0,0 5.09091,1.09091 8.90909,10.1818 8.36364,12.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <objectgroup draworder="index" id="2">
   <object id="2" x="-0.181818" y="9.27273">
    <polygon points="0,0 13.4545,1.45455 17.8182,-3.63636 31.6364,-0.909091 32.7273,22.9091 9.27273,23.0909 11.2727,20.3636 5.45455,11.4545 0.727273,10"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.7273" y="32.1818">
    <polygon points="0,0 -1.81818,-4.90909 -6.18182,-8 -6.54545,-10.5455 -12.9091,-15.2727 -2.18182,-2.90909 -4.72727,0"/>
   </object>
   <object id="2" x="0.181818" y="10.3636">
    <polygon points="0,0 33.0909,0 31.2727,-10.9091 -0.909091,-11.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="22">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="-0.545455">
    <polygon points="0,0 32.9091,32.7273 9.63636,32.7273 8.36364,26.1818 -0.909091,19.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="23">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.9091" y="32.3636">
    <polygon points="0,0 2.36364,-7.45455 10,-13.0909 0,-8.90909 -1.63636,-0.909091"/>
   </object>
   <object id="2" x="19.0909" y="-0.545455">
    <polygon points="0,0 0.363636,10.3636 12.7273,11.6364 13.0909,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="28" terrain=",,,2"/>
 <tile id="29" terrain=",,2,2"/>
 <tile id="30" terrain=",,2,"/>
 <tile id="40" terrain=",,,3"/>
 <tile id="41" terrain=",,3,3"/>
 <tile id="42" terrain=",,3,"/>
 <tile id="52" terrain=",,,4"/>
 <tile id="53" terrain=",,4,4"/>
 <tile id="54" terrain=",,4,"/>
 <tile id="60" terrain=",,,36"/>
 <tile id="61" terrain=",,36,36"/>
 <tile id="62" terrain=",,36,"/>
 <tile id="63" terrain="36,,,36"/>
 <tile id="67" terrain="0,0,,0"/>
 <tile id="68" terrain=",0,,0"/>
 <tile id="69" terrain="0,0,0,0"/>
 <tile id="70" terrain="0,,0,"/>
 <tile id="71" terrain="0,0,0,"/>
 <tile id="76">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.27273" y="31.6364">
    <polygon points="0,0 19.2727,-22.7273 17.6364,-8.90909 22.1818,0.909091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="77">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22" y="1.45455">
    <polygon points="0,0 -7.45455,9.45455 -23.2727,8.36364 -21.4545,-2"/>
   </object>
   <object id="2" x="16.7273" y="8.72727">
    <polygon points="0,0 -1.09091,13.4545 4,24.7273 9.09091,16.3636 3.63636,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.8182" y="0.181818">
    <polygon points="0,0 -7.09091,5.09091 1.63636,23.4545 -1.27273,32.1818 -23.6364,32 -24.3636,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="79" terrain="1,1,,1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="8.90909">
    <polygon points="0,0 11.8182,2.90909 22.3636,22.9091 -1.63636,22.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="80" terrain=",1,,1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22" y="32.1818">
    <polygon points="0,0 3.81818,-8.18182 -6,-26.5455 -0.363636,-32.1818 -7.09091,-25.8182 -5.63636,-9.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="81" terrain="1,1,1,1"/>
 <tile id="82" terrain="1,,1,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="11.4545" y="-1.09091">
    <polyline points="0,0 1.09091,13.4545 5.09091,17.6364 1.81818,21.4545 1.63636,24.7273 -2.18182,29.4545 -1.09091,35.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="83" terrain="1,1,1,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="7.27273" y="32.3636">
    <polygon points="0,0 9.63636,-6.54545 12.3636,-17.6364 25.2727,-24.5455 24.9091,0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="85">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.63636" y="32.3636">
    <polygon points="0,0 4.18182,-5.81818 3.63636,-11.2727 5.63636,-14 0.545455,-22.5455 2,-27.4545 -1.63636,-33.2727 23.0909,-32.5455 24.1818,-20.7273 3.45455,-21.4545 7.63636,-11.0909 4.18182,-1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="86">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.90909" y="32.7273">
    <polygon points="0,0 -0.181818,-4 4.72727,-6.18182 3.27273,-12.9091 5.09091,-14.5455 1.27273,-24.9091 23.8182,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="87">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="18.5455">
    <polyline points="0,0 2,-2.54545 8.54545,0.363636 15.6364,0 20,-4.36364 28.3636,-0.727273 32.7273,0"/>
   </object>
   <object id="2" x="32.7273" y="11.4545">
    <polygon points="0,0 -14.3636,-1.45455 -13.4545,-12.1818 -0.363636,-11.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="91" terrain="2,2,,2"/>
 <tile id="92" terrain=",2,,2"/>
 <tile id="93" terrain="2,2,2,2"/>
 <tile id="94" terrain="2,,2,"/>
 <tile id="95" terrain="2,2,2,"/>
 <tile id="103" terrain="3,3,,3"/>
 <tile id="104" terrain=",3,,3"/>
 <tile id="105" terrain="3,3,3,3"/>
 <tile id="106" terrain="3,,3,"/>
 <tile id="107" terrain="3,3,3,"/>
 <tile id="115" terrain="4,4,,4"/>
 <tile id="116" terrain=",4,,4"/>
 <tile id="117" terrain="4,4,4,4"/>
 <tile id="118" terrain="4,,4,"/>
 <tile id="119" terrain="4,4,4,"/>
 <tile id="124" terrain=",36,,36"/>
 <tile id="125" terrain="36,36,36,36"/>
 <tile id="126" terrain="36,,36,"/>
 <tile id="127" terrain=",36,36,"/>
 <tile id="132" terrain=",0,,"/>
 <tile id="133" terrain="0,0,,"/>
 <tile id="134" terrain="0,,,"/>
 <tile id="140">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.09091" y="30.9091">
    <polygon points="0,0 23.6364,-22 34.1818,-21.6364 33.2727,1.63636 0.909091,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="141">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="10">
    <polygon points="0,0 15.6364,-0.545455 23.0909,-10.1818 0.727273,-10.3636"/>
   </object>
   <object id="2" x="32.5455" y="10.9091">
    <polygon points="0,0 -10.9091,-2.18182 -13.6364,-6.36364 -14.3636,21.6364 0.363636,21.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="142">
  <objectgroup draworder="index" id="2">
   <object id="1" x="24.1818" y="0.181818">
    <polygon points="0,0 -4.18182,4.90909 0.545455,10.9091 8,11.2727 8.36364,33.4545 -24.5455,32.7273 -24,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="143">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="11.0909">
    <polygon points="0,0 -13.2727,-4 -12.7273,-11.2727 -12.9091,-4 -18.7273,-1.09091 -32.3636,-1.81818 -32.3636,20.9091 0.363636,21.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="144" terrain=",1,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18.7273" y="32">
    <polygon points="0,0 1.63636,-22.5455 1.45455,-32.7273 3.27273,-22.3636 13.0909,-21.4545 13.2727,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="145" terrain="1,1,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="9.09091">
    <polygon points="0,0 10.5455,1.81818 14.7273,-3.27273 23.6364,-2.54545 31.0909,-1.45455 31.2727,22.9091 0.363636,22.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="146" terrain="1,,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.63636" y="0.363636">
    <polygon points="0,0 -0.363636,9.45455 -9.63636,8.90909 -9.45455,32 1.27273,31.2727 2.18182,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="147">
  <objectgroup draworder="index" id="2">
   <object id="1" x="33.0909" y="9.45455">
    <polygon points="0,0 -22,-1.09091 -20,-10.5455 -23.2727,-10 -24.1818,-1.09091 -32.7273,0 -33.2727,23.2727 -0.181818,23.2727 -0.727273,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="149">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.3636" y="32.5455">
    <polygon points="0,0 -0.181818,-22.5455 20.1818,-23.8182 19.0909,-32.5455 -2.54545,-32.7273 -3.63636,-22.3636 -12.3636,-23.2727 -12.1818,0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="150">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.81818" y="9.45455">
    <polygon points="0,0 12.7273,0 35.0909,23.0909 1.09091,22.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="151">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.90909" y="32">
    <polyline points="0,0 1.27273,-4.72727 -4.72727,-12.1818 -10.7273,-13.2727"/>
   </object>
   <object id="2" x="18.7273" y="0.363636">
    <polygon points="0,0 0.363636,8.54545 14.3636,10.7273 12.9091,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="156" terrain=",2,,"/>
 <tile id="157" terrain="2,2,,"/>
 <tile id="158" terrain="2,,,"/>
 <tile id="168" terrain=",3,,"/>
 <tile id="169" terrain="3,3,,"/>
 <tile id="170" terrain="3,,,"/>
 <tile id="180" terrain=",4,,"/>
 <tile id="181" terrain="4,4,,"/>
 <tile id="182" terrain="4,,,"/>
 <tile id="188" terrain=",36,,"/>
 <tile id="189" terrain="36,36,,"/>
 <tile id="190" terrain="36,,,"/>
 <tile id="204">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.45455" y="31.6364">
    <polygon points="0,0 17.4545,-15.8182 20,-32 31.0909,-30.9091 30.9091,1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="205">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="9.63636">
    <polygon points="0,0 18,1.27273 20,24.3636 32.3636,22.1818 31.0909,-10.1818 -0.181818,-9.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="206">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="0.545455">
    <polygon points="0,0 0.181818,31.6364 32.3636,32.1818 31.6364,-0.909091 -1.27273,-1.45455 -1.81818,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="207">
  <objectgroup draworder="index" id="2">
   <object id="1" x="17.6364" y="0.545455">
    <polygon points="0,0 -2.72727,8.18182 -18.1818,8.54545 -17.0909,31.6364 14.7273,32.3636 14.1818,-0.363636 2,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="208">
  <objectgroup draworder="index" id="2">
   <object id="1" x="19.8182" y="32.1818">
    <polygon points="0,0 -0.909091,-19.4545 -6,-21.6364 -2.54545,-34 12,-32.1818 12.5455,0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="209">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="0.909091">
    <polygon points="0,0 31.4545,-0.909091 32.5455,31.6364 0.727273,31.2727 -1.45455,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="210">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10" y="0.181818">
    <polygon points="0,0 2.54545,10.9091 5.63636,17.4545 4.72727,24.1818 5.81818,26 0.545455,30.3636 1.09091,32.5455 -9.81818,32.1818 -10.3636,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="211">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="9.09091">
    <polygon points="0,0 -21.6364,-1.09091 -22,-10.3636 -33.2727,-9.45455 -33.0909,24 -0.181818,22.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="213">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="31.0909">
    <polygon points="0,0 11.8182,1.45455 17.4545,-22.1818 32.1818,-22 32.7273,-31.2727 0,-31.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="214">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.1818" y="0.727273">
    <polygon points="0,0 1.63636,14.9091 20.7273,31.8182 -11.8182,31.8182 -12.5455,-1.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="215">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.3636" y="33.0909">
    <polyline points="0,0 2.18182,-10 11.0909,-14.1818"/>
   </object>
   <object id="2" x="-1.27273" y="9.63636">
    <polygon points="0,0 10.7273,2.54545 11.6364,-10.9091 1.27273,-10"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="268">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18.7273" y="0.545455">
    <polygon points="0,0 0.545455,8.36364 15.0909,11.0909 13.8182,-1.09091 0.909091,-1.45455"/>
   </object>
   <object id="2" x="10" y="-0.181818">
    <polyline points="0,0 13.8182,12.1818 12.9091,15.2727 17.8182,17.6364 22.5455,17.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="269">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10.7273" y="1.09091">
    <polyline points="0,0 8.36364,8.18182 12.9091,15.4545 23.0909,18.7273"/>
   </object>
   <object id="2" x="18.5455" y="8.36364">
    <polyline points="0,0 13.4545,0.727273 13.6364,-9.09091 -9.09091,-9.27273 -7.09091,-5.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="270">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.81818" y="-0.181818">
    <polygon points="0,0 1.45455,4.90909 12.5455,9.81818 12.5455,15.8182 21.0909,19.6364 22.5455,19.8182 22,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="271">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.5455" y="8.18182">
    <polygon points="0,0 -16.9091,-2.90909 -9.81818,8.18182 0.363636,12.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="272">
  <objectgroup draworder="index" id="2">
   <object id="1" x="18" y="1.09091">
    <polygon points="0,0 1.09091,9.09091 16,11.0909 14.7273,-2.18182 1.27273,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="273">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="10.3636">
    <polygon points="0,0 32.7273,-1.45455 31.4545,-11.0909 -1.27273,-11.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="274">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.727273" y="9.27273">
    <polygon points="0,0 9.27273,2.90909 10.9091,-10.1818 0.545455,-10"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="275">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.363636" y="19.2727">
    <polygon points="0,0 9.45455,2.18182 17.8182,-7.63636 17.6364,-13.2727 10,-9.09091 -0.727273,-10.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="276">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="18.7273">
    <polygon points="0,0 11.4545,2.90909 18,-7.81818 18.3636,-12.1818 24.1818,-19.4545 -0.181818,-19.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="277">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="18.3636">
    <polygon points="0,0 7.81818,2.18182 16.7273,-7.27273 16.3636,-11.8182 23.4545,-18.9091 -1.09091,-18.5455 0.909091,-8.18182 14.7273,-7.45455 8,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="278">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.909091" y="9.81818">
    <polygon points="0,0 11.4545,2.90909 10.9091,-9.81818 0.545455,-10.3636"/>
   </object>
   <object id="2" x="0.727273" y="18.7273">
    <polyline points="0,0 6.36364,1.45455 15.8182,-6.18182 13.4545,-10.7273 21.8182,-18"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="279">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="20.1818">
    <polyline points="0,0 6.18182,-3.63636 15.0909,-0.727273 20.9091,-4.36364 32,-1.09091"/>
   </object>
   <object id="2" x="10.3636" y="0.181818">
    <polygon points="0,0 -2.18182,13.6364 -10.9091,9.63636 -12.7273,-1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="328" terrain=",0,0,0"/>
 <tile id="329" terrain="0,,0,0"/>
 <tile id="340" terrain=",1,1,1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.545455" y="18.5455">
    <polyline points="0,0 9.63636,1.45455 17.6364,-6.72727 17.0909,-12 22.3636,-15.8182 22.9091,-19.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="341" terrain="1,,1,1"/>
 <tile id="343">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="10.5455">
    <polygon points="0,0 8.72727,0.545455 10.3636,-11.2727 -0.545455,-11.2727"/>
   </object>
   <object id="2" x="1.09091" y="18.5455">
    <polyline points="0,0 3.09091,3.81818 8.90909,9.81818 8.18182,13.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="352" terrain=",2,2,2"/>
 <tile id="353" terrain="2,,2,2"/>
 <tile id="364" terrain=",3,3,3"/>
 <tile id="365" terrain="3,,3,3"/>
 <tile id="376" terrain=",4,4,4"/>
 <tile id="377" terrain="4,,4,4"/>
 <tile id="380" terrain="36,36,36,"/>
 <tile id="381" terrain="36,36,,36"/>
 <tile id="382" terrain="36,,36,36"/>
 <tile id="383" terrain=",36,36,36"/>
 <tile id="444" terrain=",,,37"/>
 <tile id="445" terrain=",,37,37"/>
 <tile id="446" terrain=",,37,"/>
 <tile id="447" terrain="37,,,37"/>
 <tile id="508" terrain=",37,,37"/>
 <tile id="509" terrain="37,37,37,37"/>
 <tile id="510" terrain="37,,37,"/>
 <tile id="511" terrain=",37,37,"/>
 <tile id="572" terrain=",37,,"/>
 <tile id="573" terrain="37,37,,"/>
 <tile id="574" terrain="37,,,"/>
 <tile id="764" terrain="37,37,37,"/>
 <tile id="765" terrain="37,37,,37"/>
 <tile id="766" terrain="37,,37,37"/>
 <tile id="767" terrain=",37,37,37"/>
 <tile id="772" terrain=",,,5"/>
 <tile id="773" terrain=",,5,5"/>
 <tile id="774" terrain=",,5,"/>
 <tile id="784" terrain=",,,6"/>
 <tile id="785" terrain=",,6,6"/>
 <tile id="786" terrain=",,6,"/>
 <tile id="793" terrain=",,0,"/>
 <tile id="796" terrain=",,,7"/>
 <tile id="797" terrain=",,7,7"/>
 <tile id="798" terrain=",,7,"/>
 <tile id="808" terrain=",,,8"/>
 <tile id="809" terrain=",,8,8"/>
 <tile id="810" terrain=",,8,"/>
 <tile id="816">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.6364" y="32">
    <polygon points="0,0 -1.09091,-4.18182 5.09091,-10.1818 8.54545,-12.7273 8.36364,-14.7273 5.27273,-14.3636 2.72727,-10.1818 0.727273,-7.45455 -2.54545,-4.36364 -3.81818,0.363636"/>
   </object>
   <object id="3" x="0.363636" y="32.5455">
    <polygon points="0,0 31.8182,-31.2727 30.7273,-32.3636 -0.545455,-2.36364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="817">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.8182" y="31.8182">
    <polygon points="0,0 0.545455,-4.72727 4.90909,-8.36364 8,-9.63636 8,-12.5455 4.90909,-9.81818 1.09091,-9.45455 0.181818,-8.54545 -0.727273,-4 -1.63636,-3.45455 -2.36364,-1.81818 -2.54545,-0.181818"/>
   </object>
   <object id="2" x="-0.545455" y="9.63636">
    <polygon points="0,0 13.2727,1.63636 20.3636,-2.18182 32.9091,0 33.2727,-10.3636 -0.363636,-10.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="818">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.7273" y="31.8182">
    <polygon points="0,0 2,-5.63636 4.90909,-8.36364 7.81818,-9.09091 9.27273,-12.1818 7.27273,-11.4545 6.54545,-10 2.72727,-9.09091 0.181818,-7.27273 -0.363636,-2"/>
   </object>
   <object id="2" x="22.7273" y="31.8182">
    <polygon points="0,0 -22.9091,0 -21.0909,-32.9091 10.1818,-31.4545 9.45455,-10.9091 4.72727,-8.36364 1.63636,-4.36364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="819">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="9.81818">
    <polygon points="0,0 14.3636,0.545455 18.3636,-3.45455 22,-2.72727 24.9091,0.181818 31.0909,-0.727273 25.2727,-2 21.0909,-4.54545 15.0909,-3.63636 14.1818,-1.27273"/>
   </object>
   <object id="2" x="24" y="32">
    <polygon points="0,0 1.09091,-6.18182 4.90909,-8.36364 8.54545,-12.7273 6.90909,-12.3636 5.27273,-10.1818 0.909091,-9.63636 0.545455,-8.18182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="820" terrain=",,,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="24.1818" y="31.2727">
    <polygon points="0,0 0.363636,-4.36364 3.45455,-7.27273 6.72727,-8 8,-12 6.36364,-11.2727 4.90909,-9.81818 0.363636,-9.09091 -0.909091,-7.45455 -0.545455,-3.63636 -2.54545,-2.54545 -3.09091,-0.363636 -2.54545,0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="821" terrain=",,9,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="19.0909">
    <polygon points="0,0 4.54545,-2.72727 7.09091,-0.727273 15.8182,-1.27273 20.7273,-4.18182 26,-3.09091 28.5455,-0.545455 31.8182,-1.45455 32.1818,0.545455 28.5455,0.909091 23.6364,-2.54545 20.5455,-2.36364 17.4545,0 6.36364,0.363636 4,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="822" terrain=",,9,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.7273" y="32">
    <polygon points="0,0 -2.54545,-4.72727 -5.45455,-6.36364 -6.36364,-9.45455 -10.3636,-11.6364 -10.3636,-13.2727 -13.4545,-14.1818 -9.27273,-13.6364 -8.18182,-11.4545 -5.45455,-10.1818 -4.90909,-7.81818 -2.18182,-5.63636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="823">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.81818" y="33.0909">
    <polygon points="0,0 1.63636,-3.81818 -2.72727,-10.5455 -9.45455,-15.6364 -9.63636,-13.6364 -0.181818,-5.27273 -1.45455,-1.27273"/>
   </object>
   <object id="2" x="-0.181818" y="9.45455">
    <polygon points="0,0 12.9091,1.45455 19.6364,-2.90909 23.6364,-0.909091 32,-0.909091 32.5455,22.5455 11.6364,22.5455 10.9091,17.8182 0.727273,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="824">
  <objectgroup draworder="index" id="3">
   <object id="2" x="-0.545455" y="19.2727">
    <polygon points="0,0 4.36364,2.36364 8.54545,7.81818 10.7273,8.90909 8.72727,12.9091 13.4545,13.0909 12.7273,8 3.09091,-0.909091 1.63636,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="825">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.5455" y="32.7273">
    <polygon points="0,0 0.181818,-3.63636 -9.45455,-14.7273 -13.8182,-15.2727 -12.5455,-12.9091 -6,-8.18182 -2.90909,-4 -4.18182,-0.909091"/>
   </object>
   <object id="2" x="0.545455" y="10">
    <polygon points="0,0 12.3636,1.63636 17.2727,-2.18182 32,-0.545455 31.8182,-10 -0.727273,-10.3636 -0.181818,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="826">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.09091" y="1.45455">
    <polygon points="0,0 30.5455,29.4545 10.1818,31.0909 10.7273,27.0909 2.72727,18.3636 -0.909091,17.6364 -1.63636,-1.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="827">
  <objectgroup draworder="index" id="2">
   <object id="1" x="24.1818" y="33.2727">
    <polygon points="0,0 0.545455,-7.45455 9.45455,-10.9091 8.54545,-14.9091 -1.45455,-10.5455 -3.81818,-0.181818"/>
   </object>
   <object id="2" x="18.3636" y="0">
    <polygon points="0,0 1.45455,9.63636 15.2727,12.1818 13.4545,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="828" terrain=",,,38"/>
 <tile id="829" terrain=",,38,38"/>
 <tile id="830" terrain=",,38,"/>
 <tile id="831" terrain="38,,,38"/>
 <tile id="835" terrain="5,5,,5"/>
 <tile id="836" terrain=",5,,5"/>
 <tile id="837" terrain="5,5,5,5"/>
 <tile id="838" terrain="5,,5,"/>
 <tile id="839" terrain="5,5,5,"/>
 <tile id="847" terrain="6,6,,6"/>
 <tile id="848" terrain=",6,,6"/>
 <tile id="849" terrain="6,6,6,6"/>
 <tile id="850" terrain="6,,6,"/>
 <tile id="851" terrain="6,6,6,"/>
 <tile id="859" terrain="7,7,,7"/>
 <tile id="860" terrain=",7,,7"/>
 <tile id="861" terrain="7,7,7,7"/>
 <tile id="862" terrain="7,,7,"/>
 <tile id="863" terrain="7,7,7,"/>
 <tile id="871" terrain="8,8,,8"/>
 <tile id="872" terrain=",8,,8"/>
 <tile id="873" terrain="8,8,8,8"/>
 <tile id="874" terrain="8,,8,"/>
 <tile id="875" terrain="8,8,8,"/>
 <tile id="880">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.1818" y="32.9091">
    <polygon points="0,0 -0.727273,-4.72727 -2.36364,-6 -3.63636,-13.6364 -0.909091,-24.5455 -22.9091,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="881">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.7273" y="32.1818">
    <polygon points="0,0 2,-7.09091 1.81818,-12.7273 0.181818,-17.8182 -5.45455,-25.8182 -1.09091,-32.9091 -6.72727,-25.2727 -6.54545,-14.9091 -5.63636,-10 -3.63636,-3.81818"/>
   </object>
   <object id="2" x="15.6364" y="10.7273">
    <polygon points="0,0 -15.8182,-0.363636 -15.4545,-10.9091 8,-11.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="882">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.0909" y="-0.363636">
    <polygon points="0,0 -6.54545,5.09091 -6.54545,10.5455 0.545455,16.5455 1.81818,21.0909 2.72727,23.6364 0.181818,28.5455 0,32 -2.90909,32 -5.27273,26.5455 -5.81818,22.3636 -7.09091,18.1818 -7.81818,13.8182 -8,8.72727 -6.54545,4.18182"/>
   </object>
   <object id="2" x="20.7273" y="32.5455">
    <polygon points="0,0 -20.3636,-0.181818 -21.2727,-33.4545 0.909091,-32.7273 -3.45455,-29.4545 -5.27273,-19.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="883" terrain="9,9,,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.0909" y="31.6364">
    <polygon points="0,0 -0.909091,-3.81818 -4.54545,-10.3636 -6,-14 -10,-14.9091 -10.3636,-20.5455 -14,-21.2727 -17.2727,-22.1818 -23.0909,-23.6364 -23.0909,-22.9091 -11.2727,-20 -10.5455,-13.8182 -6,-12.1818 -1.27273,-2.18182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="884" terrain=",9,,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="22.3636" y="32.3636">
    <polygon points="0,0 0.181818,-4.36364 3.09091,-7.81818 3.09091,-10.3636 -0.909091,-19.0909 -5.27273,-25.4545 -6.36364,-27.4545 -1.09091,-32.1818 -7.63636,-23.6364 -6.36364,-15.0909 -4.72727,-5.81818 -2.18182,-2.72727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="885" terrain="9,9,9,9"/>
 <tile id="886" terrain="9,,9,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.63636" y="31.6364">
    <polygon points="0,0 -0.727273,-4.54545 4,-4.72727 4.36364,-16.3636 1.81818,-18.1818 1.09091,-24 1.63636,-27.2727 -2.54545,-31.4545 -0.727273,-31.8182 3.27273,-30.7273 4.18182,-23.0909 3.27273,-18.5455 7.09091,-16.1818 6.54545,-7.81818 5.63636,-3.45455 3.81818,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="887" terrain="9,9,9,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8" y="32.3636">
    <polygon points="0,0 4,-1.27273 8.72727,-6.18182 10.1818,-11.4545 13.4545,-16.5455 19.8182,-19.6364 20.9091,-22.5455 24.5455,-23.4545 23.4545,-24.1818 20,-23.4545 18.1818,-20.7273 11.0909,-17.6364 8.18182,-12.1818 7.63636,-7.09091 0.909091,-1.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="888">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.54545" y="32">
    <polygon points="0,0 1.09091,-3.81818 3.81818,-4.72727 3.81818,-10.7273 7.45455,-12.5455 5.09091,-16.5455 1.81818,-21.2727 4.54545,-23.6364 3.45455,-26.5455 -0.545455,-31.6364 4.90909,-31.8182 6.72727,-22.9091 4.90909,-20.3636 9.63636,-11.6364 5.27273,-9.09091 5.81818,-4.54545 3.09091,-1.81818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="889">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.09091" y="32.1818">
    <polygon points="0,0 0.545455,-4.36364 5.09091,-6.18182 3.09091,-11.4545 5.81818,-13.2727 4.54545,-16.5455 2.54545,-20.9091 2.36364,-29.4545 -1.27273,-33.4545 3.63636,-29.2727 5.27273,-18 6.90909,-15.6364 6.54545,-3.27273 4.36364,-0.181818"/>
   </object>
   <object id="2" x="14.9091" y="11.2727">
    <polygon points="0,0 4.54545,-4.36364 17.6364,-1.63636 16.3636,-11.2727 -5.09091,-12.5455 -1.63636,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="890">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.36364" y="32.5455">
    <polygon points="0,0 1.45455,-4.18182 5.09091,-5.27273 5.63636,-9.27273 2.90909,-10.1818 6.72727,-12.9091 6.18182,-16 2.54545,-23.6364 24.5455,0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="891">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="20.1818">
    <polygon points="0,0 5.09091,-2.54545 9.63636,0.545455 16.5455,-0.181818 18.7273,-3.81818 26.7273,-3.45455 28.5455,-1.45455 33.2727,-0.363636 32.7273,-2.54545 28,-2.72727 26.9091,-4.72727 17.2727,-4.90909 16,-2.72727 7.27273,-2.72727 6.18182,-3.63636 1.09091,-4"/>
   </object>
   <object id="2" x="17.6364" y="0.181818">
    <polygon points="0,0 1.81818,11.0909 15.4545,10.9091 13.8182,-1.63636 -1.45455,-1.09091 -0.545455,1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="892" terrain=",38,,38"/>
 <tile id="893" terrain="38,38,38,38"/>
 <tile id="894" terrain="38,,38,"/>
 <tile id="895" terrain=",38,38,"/>
 <tile id="900" terrain=",5,,"/>
 <tile id="901" terrain="5,5,,"/>
 <tile id="902" terrain="5,,,"/>
 <tile id="912" terrain=",6,,"/>
 <tile id="913" terrain="6,6,,"/>
 <tile id="914" terrain="6,,,"/>
 <tile id="924" terrain=",7,,"/>
 <tile id="925" terrain="7,7,,"/>
 <tile id="926" terrain="7,,,"/>
 <tile id="936" terrain=",8,,"/>
 <tile id="937" terrain="8,8,,"/>
 <tile id="938" terrain="8,,,"/>
 <tile id="944">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="10">
    <polygon points="0,0 -12,-0.545455 -32.9091,21.6364 -1.27273,22.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="945">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="10.5455">
    <polygon points="0,0 -11.4545,0.181818 -12.3636,-6.36364 -8.72727,-10.7273 -12.7273,-10.7273 -14.5455,21.8182 -0.545455,20.9091"/>
   </object>
   <object id="2" x="18.3636" y="7.63636">
    <polygon points="0,0 -2,0.181818 -3.09091,2.54545 -5.27273,3.27273 -12.9091,3.27273 -14.7273,1.63636 -18.1818,2.36364 -18.7273,-7.45455 1.27273,-7.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="946">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32" y="9.81818">
    <polygon points="0,0 -5.09091,-0.181818 -9.09091,-1.27273 -10.7273,-2.90909 -11.6364,-5.63636 -9.63636,-7.63636 -8.72727,-8.90909 -8.54545,-10.1818 -12.9091,-9.63636 -14,22.7273 0,22"/>
   </object>
   <object id="2" x="17.8182" y="32.3636">
    <polygon points="0,0 -18,0.181818 -17.8182,-32.3636 1.27273,-32 -1.27273,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="947">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.1818" y="10">
    <polygon points="0,0 -13.4545,1.27273 -13.6364,-5.63636 -11.4545,-7.81818 -12,-10.9091 -8.18182,-10 -10.5455,-7.09091 -11.0909,-4 -5.45455,-1.27273"/>
   </object>
   <object id="2" x="20.5455" y="5.45455">
    <polygon points="0,0 -4.36364,2.36364 -6,3.81818 -10.3636,4.90909 -13.8182,4.36364 -15.6364,3.63636 -20,4 -20.9091,3.81818 -20.7273,4.18182 -13.4545,5.45455 -8.90909,6.18182 -3.45455,3.27273"/>
   </object>
   <object id="3" x="0.363636" y="10.1818">
    <polygon points="0,0 -1.63636,22.1818 31.8182,21.8182 31.4545,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="948" terrain=",9,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.8182" y="10.1818">
    <polygon points="0,0 -12.3636,-0.181818 -13.8182,21.2727 0,21.8182"/>
   </object>
   <object id="2" x="23.8182" y="10.3636">
    <polygon points="0,0 -2.54545,-5.09091 -1.09091,-8.54545 -1.09091,-10.3636 -4.36364,-11.0909 -4,-4.72727 -4.18182,-1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="949" terrain="9,9,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.363636" y="9.63636">
    <polygon points="0,0 12.3636,-0.181818 16.5455,-2.90909 21.0909,-3.27273 25.2727,-0.545455 31.0909,-1.45455 31.6364,21.8182 0.181818,21.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="950" terrain="9,,,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="10">
    <polygon points="0,0 7.81818,-0.181818 10.1818,-0.727273 10.1818,-10.1818 12.1818,-9.45455 11.0909,21.8182 1.09091,21.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="951">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.5455" y="9.27273">
    <polygon points="0,0 -18.3636,-1.09091 -19.4545,0.727273 -21.8182,1.09091 -20.7273,0.181818 -20.9091,-5.81818 -19.6364,-6.90909 -19.8182,-9.63636 -24.1818,-9.63636 -22.9091,-5.27273 -23.2727,-2.18182 -26,0.545455 -21.8182,0.909091 -32.3636,0 -32.7273,23.4545 0.363636,23.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="952">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10" y="0.545455">
    <polygon points="0,0 -0.363636,8.90909 -9.81818,9.09091 -9.63636,31.6364 22,31.6364 21.8182,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="953">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.27273" y="9.27273">
    <polygon points="0,0 10,-0.363636 10.7273,22.3636 -2,22.5455 -1.81818,-1.09091"/>
   </object>
   <object id="2" x="12.5455" y="11.4545">
    <polygon points="0,0 9.09091,-4.36364 19.6364,-1.45455 18.9091,-11.8182 -4.36364,-12.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="954">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.909091" y="10.1818">
    <polygon points="0,0 11.8182,-0.181818 32.7273,20.3636 33.4545,22.5455 2.36364,22.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="955">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.09091" y="32.1818">
    <polygon points="0,0 0.909091,-4.18182 -2.18182,-7.09091 -2.90909,-9.81818 -7.27273,-12.9091 -9.27273,-13.2727 -8.72727,-14.7273 -5.81818,-14.1818 -1.27273,-7.63636 2.90909,-2.90909 4.18182,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="956" terrain=",38,,"/>
 <tile id="957" terrain="38,38,,"/>
 <tile id="958" terrain="38,,,"/>
 <tile id="1008">
  <objectgroup draworder="index" id="2">
   <object id="1" x="21.6364" y="32.9091">
    <polygon points="0,0 -0.181818,-3.09091 -5.27273,-6.90909 -4,-8.90909 -4.54545,-13.2727 -6.72727,-15.4545 -1.09091,-21.8182 -2.72727,-26.9091 -0.545455,-32.9091 10.9091,-32.7273 10,-1.09091"/>
   </object>
   <object id="2" x="0.727273" y="32.1818">
    <polygon points="0,0 14.1818,-14.3636 20.7273,0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1009">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="10.3636">
    <polygon points="0,0 19.4545,0.727273 20.7273,23.6364 32.7273,22.5455 31.2727,-10.9091 0,-11.2727 0.909091,-1.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1011">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="9.45455">
    <polygon points="0,0 14.9091,0.727273 18,-10 32.3636,-9.27273 31.8182,22.5455 0.545455,22.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1012">
  <objectgroup draworder="index" id="2">
   <object id="1" x="19.2727" y="31.4545">
    <polygon points="0,0 0.909091,-15.4545 -4.36364,-21.8182 -2.54545,-32.7273 12.3636,-31.4545 12.7273,0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1013">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.545455" y="31.6364">
    <polygon points="0,0 32.5455,0.727273 32,-32.1818 -2,-31.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1014">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10.1818" y="31.6364">
    <polygon points="0,0 5.27273,-4.90909 5.81818,-15.0909 1.63636,-19.6364 1.63636,-24.1818 4.18182,-25.0909 0.909091,-31.8182 -10.5455,-31.6364 -10.3636,1.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1015">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.3636" y="8.36364">
    <polygon points="0,0 -15.6364,-1.45455 -20.5455,1.09091 -21.4545,-5.63636 -22,-8.54545 -16.7273,9.81818 -19.4545,13.4545 -16.1818,18.1818 -21.8182,22 -21.4545,23.8182 -0.727273,23.2727"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1016">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="32.1818">
    <polygon points="0,0 32.3636,0 31.8182,-32.9091 -0.545455,-32.7273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1017">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.63636" y="31.4545">
    <polygon points="0,0 5.81818,-5.45455 6.18182,-14.7273 -0.363636,-20.5455 8.18182,-24.5455 22.7273,-22.5455 22.7273,-31.8182 -10.1818,-31.8182 -9.81818,0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1018">
  <objectgroup draworder="index" id="3">
   <object id="2" x="10.5455" y="0.545455">
    <polygon points="0,0 2,7.63636 3.63636,14.9091 21.6364,29.8182 21.6364,31.6364 -0.545455,31.2727 4.72727,26.7273 3.63636,20.9091 5.63636,17.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1019">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.8182" y="33.0909">
    <polygon points="0,0 0.181818,-5.81818 3.27273,-9.09091 7.81818,-11.0909 9.09091,-14.5455 5.45455,-11.8182 0.909091,-11.2727 -0.909091,-9.81818 -0.727273,-5.09091 -2.54545,-1.81818"/>
   </object>
   <object id="2" x="1.09091" y="10.7273">
    <polygon points="0,0 9.27273,2.54545 9.27273,-11.4545 -1.45455,-11.4545"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1072">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.27273" y="-0.181818">
    <polygon points="0,0 4.18182,0.727273 13.6364,12.3636 14.5455,15.4545 22.5455,17.6364 22.5455,20.5455 18.9091,19.6364 11.6364,15.8182 11.6364,11.6364"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1073">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10" y="-0.545455">
    <polygon points="0,0 0.545455,2.18182 14.5455,11.4545 13.2727,16 22.1818,17.8182 22.1818,20.9091 11.0909,16.7273 11.0909,11.6364 -1.81818,1.27273"/>
   </object>
   <object id="2" x="32" y="9.45455">
    <polygon points="0,0 -13.0909,-1.45455 -22.1818,-11.0909 0,-9.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1074">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12.1818" y="-0.181818">
    <polygon points="0,0 1.09091,2.36364 12.1818,11.0909 10.9091,14.5455 15.4545,17.6364 20.5455,18.1818 19.8182,20.5455 15.4545,19.8182 8.90909,15.8182 8.72727,11.4545 -3.27273,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1075">
  <objectgroup draworder="index" id="2">
   <object id="1" x="31.8182" y="20.1818">
    <polygon points="0,0 -10,-4.36364 -10.5455,-10.7273 -15.8182,-13.6364 0.181818,-12.1818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1076">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32.7273" y="10.5455">
    <polygon points="0,0 -13.8182,-0.909091 -13.8182,-10.9091 -0.181818,-9.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1077">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.909091" y="9.09091">
    <polygon points="0,0 31.6364,0 31.2727,-9.27273 -1.63636,-9.45455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1078">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.81818" y="0.727273">
    <polygon points="0,0 -0.909091,11.4545 -10,9.81818 -10.7273,-2.90909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1079">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="9.27273">
    <polygon points="0,0 11.2727,0.727273 16.3636,-3.27273 17.4545,2 11.6364,9.81818 5.45455,11.8182 0.363636,9.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1080">
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.27273" y="19.2727">
    <polygon points="0,0 7.09091,1.09091 15.0909,-7.63636 11.6364,-11.4545 19.4545,-15.2727 19.6364,-20.3636 17.8182,-15.6364 10.1818,-12.1818 12.5455,-6.90909 6,-0.363636"/>
   </object>
   <object id="2" x="0.181818" y="18.9091">
    <polygon points="0,0 0.727273,-20.3636 22.5455,-18.9091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1081">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.727273" y="19.6364">
    <polygon points="0,0 10.7273,0.545455 16.1818,-8.54545 17.0909,-13.2727 21.6364,-17.4545 21.8182,-20 15.2727,-11.6364 14.5455,-7.63636 8.18182,-2.54545"/>
   </object>
   <object id="2" x="15.8182" y="10">
    <polygon points="0,0 -15.0909,-0.181818 -16.1818,-10.1818 6.54545,-9.63636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1082">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0.181818" y="18.9091">
    <polygon points="0,0 6.90909,2.54545 16.5455,-6.36364 15.8182,-11.8182 22.9091,-16.5455 23.4545,-19.0909 21.0909,-17.0909 14,-11.6364 14.5455,-6.18182 10,-5.09091 9.45455,-1.27273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1083">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.909091" y="20">
    <polygon points="0,0 5.45455,-3.63636 8.54545,-0.727273 17.0909,-0.727273 22.7273,-4.72727 28.7273,-2.36364 33.2727,-1.45455 33.2727,0.545455 28.1818,0 24.7273,-2.90909 21.8182,-2.90909 18.5455,0 8.18182,0.363636 5.27273,-2.36364"/>
   </object>
   <object id="2" x="-0.545455" y="10.3636">
    <polygon points="0,0 9.27273,1.63636 11.8182,-11.2727 1.27273,-10.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1096" terrain=",5,5,5"/>
 <tile id="1097" terrain="5,,5,5"/>
 <tile id="1108" terrain=",6,6,6"/>
 <tile id="1109" terrain="6,,6,6"/>
 <tile id="1120" terrain=",7,7,7"/>
 <tile id="1121" terrain="7,,7,7"/>
 <tile id="1132" terrain=",8,8,8"/>
 <tile id="1133" terrain="8,,8,8"/>
 <tile id="1141">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.181818" y="10">
    <polyline points="0,0 33.2727,0.181818 32.5455,-11.8182 -0.727273,-11.0909 -1.27273,1.27273"/>
   </object>
   <object id="2" x="0" y="18.9091">
    <polyline points="0,0 22,-2.90909 32.9091,0.545455 29.2727,0.727273 -1.45455,0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1143">
  <objectgroup draworder="index" id="2">
   <object id="1" x="32" y="18.9091">
    <polygon points="0,0 -6.72727,-3.45455 -14.7273,-4 -22.1818,-0.363636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1144" terrain=",9,9,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="23.4545" y="0.363636">
    <polygon points="0,0 -13.8182,20.3636 -24.3636,19.4545 -23.2727,16.3636 -14.7273,17.0909"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1145" terrain="9,,9,9">
  <objectgroup draworder="index" id="2">
   <object id="1" x="11.8182" y="-0.363636">
    <polygon points="0,0 1.63636,3.81818 11.6364,10.3636 10.9091,14.7273 21.4545,18.3636 21.4545,20.3636 16.7273,19.8182 9.63636,16.7273 7.63636,9.63636 -1.81818,0.909091 -2,-0.545455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1146">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-1.09091" y="19.8182">
    <polygon points="0,0 4.18182,-4.36364 9.81818,-1.63636 27.2727,-4.18182 28.1818,-1.27273 13.4545,0.545455 6,-0.727273"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1147">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="18.5455">
    <polygon points="0,0 5.81818,0 14.1818,13.6364 7.81818,13.6364 9.27273,7.45455"/>
   </object>
   <object id="2" x="-0.363636" y="9.27273">
    <polygon points="0,0 8.90909,3.45455 11.6364,-9.81818 -0.181818,-10"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1148" terrain="38,38,38,"/>
 <tile id="1149" terrain="38,38,,38"/>
 <tile id="1150" terrain="38,,38,38"/>
 <tile id="1151" terrain=",38,38,38"/>
 <tile id="1212" terrain=",,,39"/>
 <tile id="1213" terrain=",,39,39"/>
 <tile id="1214" terrain=",,39,"/>
 <tile id="1215" terrain="39,,,39"/>
 <tile id="1276" terrain=",39,,39"/>
 <tile id="1277" terrain="39,39,39,39"/>
 <tile id="1278" terrain="39,,39,"/>
 <tile id="1279" terrain=",39,39,"/>
 <tile id="1340" terrain=",39,,"/>
 <tile id="1341" terrain="39,39,,"/>
 <tile id="1342" terrain="39,,,"/>
 <tile id="1399">
  <objectgroup draworder="index" id="2">
   <object id="1" x="10.5455" y="32">
    <polygon points="0,0 1.27273,-8.36364 11.0909,-13.8182 21.8182,-11.6364 20.9091,-24 7.09091,-25.8182 2.18182,-24.1818 1.63636,-20.9091 -4.90909,-21.4545 -10.5455,-22.7273 -10.1818,-0.181818"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1400">
  <objectgroup draworder="index" id="2">
   <object id="1" x="-0.727273" y="17.6364">
    <polygon points="0,0 12.5455,-1.81818 24.3636,11.6364 23.4545,14 32.7273,14.7273 33.6364,-10 0.909091,-9.09091"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1463">
  <objectgroup draworder="index" id="2">
   <object id="1" x="9.09091" y="32">
    <polyline points="0,0 -1.45455,-11.8182 0,-13.8182 0,-19.0909 -1.45455,-21.4545 1.45455,-32.5455"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1464">
  <objectgroup draworder="index" id="2">
   <object id="1" x="24.3636" y="31.8182">
    <polyline points="0,0 -2.72727,-11.8182 0.909091,-18.7273 -2.18182,-31.8182"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1527">
  <objectgroup draworder="index" id="2">
   <object id="1" x="8.72727" y="0.181818">
    <polyline points="0,0 -0.181818,10.3636 0,10.3636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1528">
  <objectgroup draworder="index" id="2">
   <object id="1" x="24.1818" y="7.63636">
    <polyline points="0,0 -0.727273,-7.63636"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1532" terrain="39,39,39,"/>
 <tile id="1533" terrain="39,39,,39"/>
 <tile id="1534" terrain="39,,39,39"/>
 <tile id="1535" terrain=",39,39,39"/>
 <tile id="1540" terrain=",,,10"/>
 <tile id="1541" terrain=",,10,10"/>
 <tile id="1542" terrain=",,10,"/>
 <tile id="1552" terrain=",,,11"/>
 <tile id="1553" terrain=",,11,11"/>
 <tile id="1554" terrain=",,11,"/>
 <tile id="1564" terrain=",,,12"/>
 <tile id="1565" terrain=",,12,12"/>
 <tile id="1566" terrain=",,12,"/>
 <tile id="1576" terrain=",,,13"/>
 <tile id="1577" terrain=",,13,13"/>
 <tile id="1578" terrain=",,13,"/>
 <tile id="1588" terrain=",,,14"/>
 <tile id="1589" terrain=",,14,14"/>
 <tile id="1590" terrain=",,14,"/>
 <tile id="1603" terrain="10,10,,10"/>
 <tile id="1604" terrain=",10,,10"/>
 <tile id="1605" terrain="10,10,10,10"/>
 <tile id="1606" terrain="10,,10,"/>
 <tile id="1607" terrain="10,10,10,"/>
 <tile id="1615" terrain="11,11,,11"/>
 <tile id="1616" terrain=",11,,11"/>
 <tile id="1617" terrain="11,11,11,11"/>
 <tile id="1618" terrain="11,,11,"/>
 <tile id="1619" terrain="11,11,11,"/>
 <tile id="1627" terrain="12,12,,12"/>
 <tile id="1628" terrain=",12,,12"/>
 <tile id="1629" terrain="12,12,12,12"/>
 <tile id="1630" terrain="12,,12,"/>
 <tile id="1631" terrain="12,12,12,"/>
 <tile id="1639" terrain="13,13,,13"/>
 <tile id="1640" terrain=",13,,13"/>
 <tile id="1641" terrain="13,13,13,13"/>
 <tile id="1642" terrain="13,,13,"/>
 <tile id="1643" terrain="13,13,13,"/>
 <tile id="1651" terrain="14,14,,14"/>
 <tile id="1652" terrain=",14,,14"/>
 <tile id="1653" terrain="14,14,14,14"/>
 <tile id="1654" terrain="14,,14,"/>
 <tile id="1655" terrain="14,14,14,"/>
 <tile id="1668" terrain=",10,,"/>
 <tile id="1669" terrain="10,10,,"/>
 <tile id="1670" terrain="10,,,"/>
 <tile id="1680" terrain=",11,,"/>
 <tile id="1681" terrain="11,11,,"/>
 <tile id="1682" terrain="11,,,"/>
 <tile id="1692" terrain=",12,,"/>
 <tile id="1693" terrain="12,12,,"/>
 <tile id="1694" terrain="12,,,"/>
 <tile id="1704" terrain=",13,,"/>
 <tile id="1705" terrain="13,13,,"/>
 <tile id="1706" terrain="13,,,"/>
 <tile id="1716" terrain=",14,,"/>
 <tile id="1717" terrain="14,14,,"/>
 <tile id="1718" terrain="14,,,"/>
 <tile id="1864" terrain=",10,10,10"/>
 <tile id="1865" terrain="10,,10,10"/>
 <tile id="1876" terrain=",11,11,11"/>
 <tile id="1877" terrain="11,,11,11"/>
 <tile id="1888" terrain=",12,12,12"/>
 <tile id="1889" terrain="12,,12,12"/>
 <tile id="1900" terrain=",13,13,13"/>
 <tile id="1901" terrain="13,,13,13"/>
 <tile id="1912" terrain=",14,14,14"/>
 <tile id="1913" terrain="14,,14,14"/>
 <tile id="2399" terrain=",,,19"/>
 <tile id="2400" terrain=",,19,19"/>
 <tile id="2401" terrain=",,19,19"/>
 <tile id="2402" terrain=",,19,19"/>
 <tile id="2403" terrain=",,19,19"/>
 <tile id="2404" terrain=",,19,"/>
 <tile id="2418" terrain="21,21,21,"/>
 <tile id="2419" terrain="21,21,,21"/>
 <tile id="2421" terrain="22,22,22,"/>
 <tile id="2422" terrain="22,22,,22"/>
 <tile id="2424" terrain="23,23,23,"/>
 <tile id="2425" terrain="23,23,,23"/>
 <tile id="2427" terrain="24,24,24,"/>
 <tile id="2428" terrain="24,24,,24"/>
 <tile id="2430" terrain="25,25,25,"/>
 <tile id="2431" terrain="25,25,,25"/>
 <tile id="2433" terrain=",,,15"/>
 <tile id="2434" terrain=",,15,15"/>
 <tile id="2435" terrain=",,15,15"/>
 <tile id="2436" terrain=",,15,"/>
 <tile id="2463" terrain=",19,,19"/>
 <tile id="2468" terrain="19,,19,"/>
 <tile id="2482" terrain="21,,21,21"/>
 <tile id="2483" terrain=",21,21,21"/>
 <tile id="2485" terrain="22,,22,22"/>
 <tile id="2486" terrain=",22,22,22"/>
 <tile id="2488" terrain="23,,23,23"/>
 <tile id="2489" terrain=",23,23,23"/>
 <tile id="2491" terrain="24,,24,24"/>
 <tile id="2492" terrain=",24,24,24"/>
 <tile id="2494" terrain="25,,25,25"/>
 <tile id="2495" terrain=",25,25,25"/>
 <tile id="2497" terrain=",15,,15"/>
 <tile id="2498" terrain="15,15,15,15"/>
 <tile id="2500" terrain="15,,15,"/>
 <tile id="2503" terrain=",15,15,15"/>
 <tile id="2504" terrain="15,,15,15"/>
 <tile id="2527" terrain=",19,,19"/>
 <tile id="2532" terrain="19,,19,"/>
 <tile id="2545" terrain=",,,21"/>
 <tile id="2546" terrain=",,21,21"/>
 <tile id="2547" terrain=",,21,"/>
 <tile id="2548" terrain=",,,22"/>
 <tile id="2549" terrain=",,22,22"/>
 <tile id="2550" terrain=",,22,"/>
 <tile id="2551" terrain=",,,23"/>
 <tile id="2552" terrain=",,23,23"/>
 <tile id="2553" terrain=",,23,"/>
 <tile id="2554" terrain=",,,24"/>
 <tile id="2555" terrain=",,24,24"/>
 <tile id="2556" terrain=",,24,"/>
 <tile id="2557" terrain=",,,25"/>
 <tile id="2558" terrain=",,25,25"/>
 <tile id="2559" terrain=",,25,"/>
 <tile id="2561" terrain=",15,,15"/>
 <tile id="2564" terrain="15,,15,"/>
 <tile id="2567" terrain="15,15,,15"/>
 <tile id="2568" terrain="15,15,15,"/>
 <tile id="2591" terrain=",19,,"/>
 <tile id="2596" terrain="19,,,"/>
 <tile id="2609" terrain=",21,,21"/>
 <tile id="2610" terrain="21,21,21,21"/>
 <tile id="2611" terrain="21,,21,"/>
 <tile id="2612" terrain=",22,,22"/>
 <tile id="2613" terrain="22,22,22,22"/>
 <tile id="2614" terrain="22,,22,"/>
 <tile id="2615" terrain=",23,,23"/>
 <tile id="2616" terrain="23,23,23,23"/>
 <tile id="2617" terrain="23,,23,"/>
 <tile id="2618" terrain=",24,,24"/>
 <tile id="2619" terrain="24,24,24,24"/>
 <tile id="2620" terrain="24,,24,"/>
 <tile id="2621" terrain=",25,,25"/>
 <tile id="2622" terrain="25,25,25,25"/>
 <tile id="2623" terrain="25,,25,"/>
 <tile id="2625" terrain=",15,,"/>
 <tile id="2626" terrain="15,15,,"/>
 <tile id="2627" terrain="15,15,,"/>
 <tile id="2628" terrain="15,,,"/>
 <tile id="2657" terrain="19,19,,"/>
 <tile id="2658" terrain="19,19,,"/>
 <tile id="2673" terrain=",21,,"/>
 <tile id="2674" terrain="21,21,,"/>
 <tile id="2675" terrain="21,,,"/>
 <tile id="2676" terrain=",22,,"/>
 <tile id="2677" terrain="22,22,,"/>
 <tile id="2678" terrain="22,,,"/>
 <tile id="2679" terrain=",23,,"/>
 <tile id="2680" terrain="23,23,,"/>
 <tile id="2681" terrain="23,,,"/>
 <tile id="2682" terrain=",24,,"/>
 <tile id="2683" terrain="24,24,,"/>
 <tile id="2684" terrain="24,,,"/>
 <tile id="2685" terrain=",25,,"/>
 <tile id="2686" terrain="25,25,,"/>
 <tile id="2687" terrain="25,,,"/>
 <tile id="2866" terrain="26,26,26,"/>
 <tile id="2867" terrain="26,26,,26"/>
 <tile id="2869" terrain="27,27,27,"/>
 <tile id="2870" terrain="27,27,,27"/>
 <tile id="2872" terrain="28,28,28,"/>
 <tile id="2873" terrain="28,28,,28"/>
 <tile id="2875" terrain="29,29,29,"/>
 <tile id="2876" terrain="29,29,,29"/>
 <tile id="2878" terrain="30,30,30,"/>
 <tile id="2879" terrain="30,30,,30"/>
 <tile id="2881" terrain=",,,16"/>
 <tile id="2882" terrain=",,16,16"/>
 <tile id="2883" terrain=",,16,16"/>
 <tile id="2884" terrain=",,16,"/>
 <tile id="2930" terrain="26,,26,26"/>
 <tile id="2931" terrain=",26,26,26"/>
 <tile id="2933" terrain="27,,27,27"/>
 <tile id="2934" terrain=",27,27,27"/>
 <tile id="2936" terrain="28,,28,28"/>
 <tile id="2937" terrain=",28,28,28"/>
 <tile id="2939" terrain="29,,29,29"/>
 <tile id="2940" terrain=",29,29,29"/>
 <tile id="2942" terrain="30,,30,30"/>
 <tile id="2943" terrain=",30,30,30"/>
 <tile id="2945" terrain=",16,,16"/>
 <tile id="2946" terrain="16,16,16,16"/>
 <tile id="2948" terrain="16,,16,"/>
 <tile id="2951" terrain=",16,16,16"/>
 <tile id="2952" terrain="16,,16,16"/>
 <tile id="2993" terrain=",,,26"/>
 <tile id="2994" terrain=",,26,26"/>
 <tile id="2995" terrain=",,26,"/>
 <tile id="2996" terrain=",,,27"/>
 <tile id="2997" terrain=",,27,27"/>
 <tile id="2998" terrain=",,27,"/>
 <tile id="2999" terrain=",,,28"/>
 <tile id="3000" terrain=",,28,28"/>
 <tile id="3001" terrain=",,28,"/>
 <tile id="3002" terrain=",,,29"/>
 <tile id="3003" terrain=",,29,29"/>
 <tile id="3004" terrain=",,29,"/>
 <tile id="3005" terrain=",,,30"/>
 <tile id="3006" terrain=",,30,30"/>
 <tile id="3007" terrain=",,30,"/>
 <tile id="3009" terrain=",16,,16"/>
 <tile id="3012" terrain="16,,16,"/>
 <tile id="3015" terrain="16,16,,16"/>
 <tile id="3016" terrain="16,16,16,"/>
 <tile id="3057" terrain=",26,,26"/>
 <tile id="3058" terrain="26,26,26,26"/>
 <tile id="3059" terrain="26,,26,"/>
 <tile id="3060" terrain=",27,,27"/>
 <tile id="3061" terrain="27,27,27,27"/>
 <tile id="3062" terrain="27,,27,"/>
 <tile id="3063" terrain=",28,,28"/>
 <tile id="3064" terrain="28,28,28,28"/>
 <tile id="3065" terrain="28,,28,"/>
 <tile id="3066" terrain=",29,,29"/>
 <tile id="3067" terrain="29,29,29,29"/>
 <tile id="3068" terrain="29,,29,"/>
 <tile id="3069" terrain=",30,,30"/>
 <tile id="3070" terrain="30,30,30,30"/>
 <tile id="3071" terrain="30,,30,"/>
 <tile id="3073" terrain=",16,,"/>
 <tile id="3074" terrain="16,16,,"/>
 <tile id="3075" terrain="16,16,,"/>
 <tile id="3076" terrain="16,,,"/>
 <tile id="3121" terrain=",26,,"/>
 <tile id="3122" terrain="26,26,,"/>
 <tile id="3123" terrain="26,,,"/>
 <tile id="3124" terrain=",27,,"/>
 <tile id="3125" terrain="27,27,,"/>
 <tile id="3126" terrain="27,,,"/>
 <tile id="3127" terrain=",28,,"/>
 <tile id="3128" terrain="28,28,,"/>
 <tile id="3129" terrain="28,,,"/>
 <tile id="3130" terrain=",29,,"/>
 <tile id="3131" terrain="29,29,,"/>
 <tile id="3132" terrain="29,,,"/>
 <tile id="3133" terrain=",30,,"/>
 <tile id="3134" terrain="30,30,,"/>
 <tile id="3135" terrain="30,,,"/>
 <tile id="3231" terrain=",,,20"/>
 <tile id="3232" terrain=",,20,20"/>
 <tile id="3233" terrain=",,20,20"/>
 <tile id="3234" terrain=",,20,20"/>
 <tile id="3235" terrain=",,20,20"/>
 <tile id="3236" terrain=",,20,"/>
 <tile id="3265" terrain=",,,17"/>
 <tile id="3266" terrain=",,17,17"/>
 <tile id="3267" terrain=",,17,17"/>
 <tile id="3268" terrain=",,17,"/>
 <tile id="3295" terrain=",20,,20"/>
 <tile id="3300" terrain="20,,20,"/>
 <tile id="3328" terrain=",,,17"/>
 <tile id="3329" terrain=",17,17,17"/>
 <tile id="3330" terrain="17,17,17,17"/>
 <tile id="3332" terrain="17,,17,17"/>
 <tile id="3333" terrain=",,17,"/>
 <tile id="3359" terrain=",20,,20"/>
 <tile id="3364" terrain="20,,20,"/>
 <tile id="3366" terrain=",31,31,31"/>
 <tile id="3367" terrain="31,,31,31"/>
 <tile id="3372" terrain="32,32,32,"/>
 <tile id="3373" terrain="32,32,,32"/>
 <tile id="3378" terrain=",33,33,33"/>
 <tile id="3379" terrain="33,,33,33"/>
 <tile id="3384" terrain=",34,34,34"/>
 <tile id="3385" terrain="34,,34,34"/>
 <tile id="3390" terrain=",35,35,35"/>
 <tile id="3391" terrain="35,,35,35"/>
 <tile id="3392" terrain=",17,,17"/>
 <tile id="3397" terrain="17,,17,"/>
 <tile id="3423" terrain=",20,,"/>
 <tile id="3428" terrain="20,,,"/>
 <tile id="3430" terrain="31,31,,31"/>
 <tile id="3431" terrain="31,31,31,"/>
 <tile id="3436" terrain="32,,32,32"/>
 <tile id="3437" terrain=",32,32,32"/>
 <tile id="3442" terrain="33,33,,33"/>
 <tile id="3443" terrain="33,33,33,"/>
 <tile id="3448" terrain="34,34,,34"/>
 <tile id="3449" terrain="34,34,34,"/>
 <tile id="3454" terrain="35,35,,35"/>
 <tile id="3455" terrain="35,35,35,"/>
 <tile id="3456" terrain=",17,,"/>
 <tile id="3457" terrain="17,17,,17"/>
 <tile id="3460" terrain="17,17,17,"/>
 <tile id="3461" terrain="17,,,"/>
 <tile id="3489" terrain="20,20,,"/>
 <tile id="3490" terrain="20,20,,"/>
 <tile id="3493" terrain=",,,31"/>
 <tile id="3494" terrain=",,31,31"/>
 <tile id="3495" terrain=",,31,"/>
 <tile id="3499" terrain=",,,32"/>
 <tile id="3500" terrain=",,32,32"/>
 <tile id="3501" terrain=",,32,"/>
 <tile id="3505" terrain=",,,33"/>
 <tile id="3506" terrain=",,33,33"/>
 <tile id="3507" terrain=",,33,"/>
 <tile id="3511" terrain=",,,34"/>
 <tile id="3512" terrain=",,34,34"/>
 <tile id="3513" terrain=",,34,"/>
 <tile id="3517" terrain=",,,35"/>
 <tile id="3518" terrain=",,35,35"/>
 <tile id="3519" terrain=",,35,"/>
 <tile id="3521" terrain=",17,,"/>
 <tile id="3522" terrain="17,17,,"/>
 <tile id="3523" terrain="17,17,,"/>
 <tile id="3524" terrain="17,,,"/>
 <tile id="3557" terrain=",31,,31"/>
 <tile id="3558" terrain="31,31,31,31"/>
 <tile id="3559" terrain="31,,31,"/>
 <tile id="3563" terrain=",32,,32"/>
 <tile id="3564" terrain="32,32,32,32"/>
 <tile id="3565" terrain="32,,32,"/>
 <tile id="3569" terrain=",33,,33"/>
 <tile id="3570" terrain="33,33,33,33"/>
 <tile id="3571" terrain="33,,33,"/>
 <tile id="3575" terrain=",34,,34"/>
 <tile id="3576" terrain="34,34,34,34"/>
 <tile id="3577" terrain="34,,34,"/>
 <tile id="3581" terrain=",35,,35"/>
 <tile id="3582" terrain="35,35,35,35"/>
 <tile id="3583" terrain="35,,35,"/>
 <tile id="3621" terrain=",31,,"/>
 <tile id="3622" terrain="31,31,,"/>
 <tile id="3623" terrain="31,,,"/>
 <tile id="3627" terrain=",32,,"/>
 <tile id="3628" terrain="32,32,,"/>
 <tile id="3629" terrain="32,,,"/>
 <tile id="3633" terrain=",33,,"/>
 <tile id="3634" terrain="33,33,,"/>
 <tile id="3635" terrain="33,,,"/>
 <tile id="3639" terrain=",34,,"/>
 <tile id="3640" terrain="34,34,,"/>
 <tile id="3641" terrain="34,,,"/>
 <tile id="3645" terrain=",35,,"/>
 <tile id="3646" terrain="35,35,,"/>
 <tile id="3647" terrain="35,,,"/>
 <tile id="3713" terrain=",,,18"/>
 <tile id="3714" terrain=",,18,18"/>
 <tile id="3715" terrain=",,18,18"/>
 <tile id="3716" terrain=",,18,"/>
 <tile id="3776" terrain=",,,18"/>
 <tile id="3777" terrain=",18,18,18"/>
 <tile id="3778" terrain="18,18,18,18"/>
 <tile id="3780" terrain="18,,18,18"/>
 <tile id="3781" terrain=",,18,"/>
 <tile id="3840" terrain=",18,,18"/>
 <tile id="3845" terrain="18,,18,"/>
 <tile id="3904" terrain=",18,,"/>
 <tile id="3905" terrain="18,18,,18"/>
 <tile id="3908" terrain="18,18,18,"/>
 <tile id="3909" terrain="18,,,"/>
 <tile id="3969" terrain=",18,,"/>
 <tile id="3970" terrain="18,18,,"/>
 <tile id="3971" terrain="18,18,,"/>
 <tile id="3972" terrain="18,,,"/>
</tileset>
